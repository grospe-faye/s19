/*1. Update and Debug the following codes to ES6
	use template literals
	use array/object destructuring
	use arrow function
*/
 let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

/*let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}*/
//object destructuring
introduce({
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
});

function introduce({name, age, classes}){

	//Note: You can destructure objects inside functions.
	/*console.log("Hi! " + "I'm " + student.name + " ." + " I am " + student.ages + " years old.");
	console.log("I study the following courses" + classes);*/

	//template literals
	console.log(`Hi! I'm ${name}. I am ${age} years old.`);
	console.log(`I study the following courses ${classes}`);
}

//Arrow function
/*function getCube(num){
	console.log(Math.pow(num,3));
}*/
const getCube = (num) => Math.pow(num, 3);
let cube = getCube(3);
console.log(cube);

let numArr = [15,16,32,21,21,2];
/*numArr.forEach(function(num){
	console.log(num);
})*/
numArr.forEach(num => console.log(num));

/*let numSquared = numArr.map(function(num){
	return Math.pow(num, 2);
})
console.log(numSquared);*/

const numSquared = numArr.map(num => Math.pow(num, 2));
console.log(numSquared);


/*2. Create a class constructor able to receive 3 arguments
		- it should be able to receive two strings and a number
		- Using the this keyword assign properties:
			name,
			breed,
			dogAge = <7 * human years>
				- assign the parameters as values to each property.
*/
class Dog{
	constructor(name, breed, dogAge, humanYears){
		this.name = name;
		this.breed = breed;
		this.dogAge = 7 * humanYears;
		this.humanYears = humanYears;
	}
};
/*3. Create 2 new objects using our class constructor
	This constructor shoule be able to create Dog objects.
	Log the 2 new Dog objects in the console or alert.*/
let dog1 = new Dog('Max', 'Beagle', 2, 10);
console.log(dog1);
let dog2 = new Dog('Macho', 'Aspin', 2, 5);
console.log(dog2);
